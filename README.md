## prepare directory, folder, etc

1. Make folder on directory
2. Make server in directory
3. Create i.e. “app.js” on server dir: touch app.js

# prepare git

4. Prep a git repo such as "bitbucket" for this new folder

5. Execute git init at main folder level
6. Followed by: git add .
7. git commit -m “remarks”
8. git remote add origin https://rob1nxu@bitbucket.org/rob1nxu/testday01.git
9. git push -u origin master


# in case the origin is wrong, 
1. a. check: git remote -v
1. if really wrong, execute:
1. b. git remote remove origin
1. c. then re-assign the correct origin

6. Check remote repo. Make sure the master document shows at source

# install global utility 

7. npm install -g nodemon
8. npm install -g bower (-g means global, one time installation is required)

# run express 

1. Run on terminal: npm init (make sure it points to the server/app.js)
1. Run Express:
1. npm install express
1. npm install express —save

# create client folder

1. Create client folder with,
1. Index.html
1. css
1. Custom JS

# activate express on app.js

1. code in:

   ```console.log("starting...");```
    ```var express = require("express");```
    ```var app = express();```
    ```console.log(__dirname);```
    ```app.use(express.static(__dirname + "/../client/")); ```
    ```const NODE_PORT = process.env.NODE_PORT || 3000; ```

    ```app.listen (NODE_PORT, function() {

    console.log("Web App started at " + NODE_PORT);

    });```



# assign port and its fallback

1. Code in Express on app.js with: 
    const NODE_PORT = process.env.NODE_PORT || 3000;

1. execute on terminal:  
1. export NODE_PORT=3001
 
 1. nodemon

# test the app on browser

1. test app on browser:
    localhost:3001

# install client side ux package manager ---> run bower init on terminal(creates bower.json)

1. create .bowerrc in server folder with the following,
{
 "directory": "client/bower_components" 
}

1. install angular 
    bower install angular --save

1. install bootstrap
    bower install bootstrap --save

1. run nodemon

notes:

package json is for express server side dependencies

bower json is for ux client side dependencies. eg angular, bootstrap

in .bowerrc,  
"directory": "client/bower_components" // this is a resource configuration file redirects all ux ui component to client
it cannot contain remarks!


exlcude the following in .gitignore so that these files will not be uploaded to repo
1.node_modules
2.client/bower_components


17fsf3_rxumail
passowrd1234